/**
 * @jsx React.DOM
 */

'use strict';
var $ = require('jquery');
var React = require('react');
var DefaultLayout = require('../layouts/DefaultLayout.jsx');

var CmsPage = React.createClass({
  getDefaultProps() {
    return {
      layout: DefaultLayout,
      title: "Content API example",
      url: "api/contents.json?type=global",
      loading: "Waiting for response..."
    };
  },
  getInitialState: function() {
    return {
      settings: null
    };
  },
  componentDidMount: function() {
    $.get(this.props.url, function(result) {
      if (this.isMounted()) {
        this.setState({
          settings: result[0]
        });
      }
    }.bind(this));
  },
  render() {
  	var settings = this.state.settings? this.state.settings : this.props.loading;
    this.props.title = this.state.settings? this.state.settings.title : this.props.title
    return (
  		
      <div className="cms clearfix">
        <div className="jumbotron">
        <div className="container text-center">
          <h1>React / Fluxxor / SF</h1>
          <p>Complex web apps made easy</p>
        </div>
      </div>
        <h3>{this.props.title}</h3>
        {settings}
      </div>
  	);
  }
});

module.exports = CmsPage;

