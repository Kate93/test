BuzzActions = require('./BuzzActions');
ContentActions = require('./ContentActions');

module.exports = {
  buzz: BuzzActions,
  content: ContentActions
};