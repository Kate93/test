var Fluxxor = require("fluxxor");

var ContentConstants = require('../constants/ContentConstants');

function convertImage(item) {
  return {
    src: item.image,
    title: item.header,
    text: item.bodytext
  }
}

function extractGallery(data) {
  if (_.isArray(data.colPos0)) {
    return _.map(data.colPos0, convertImage);
  } else {
    return [convertImage(data.colPos0)]
  }
}

var StartPageStore = Fluxxor.createStore({
  initialize: function() {
    this.loading = false;
    this.adding = false;
    this.error = false;
    this.gallery = [];

    this.bindActions(
      ContentConstants.LOAD_CONTENT_START, this.onLoad,
      ContentConstants.LOAD_CONTENT_START_SUCCESS, this.onLoadSuccess,
      ContentConstants.LOAD_CONTENT_START_FAIL, this.onLoadFail
    );
  },

  onLoad: function() {
    this.loading = true;
    this.error = null;
    this.emit("change");
  },

  onLoadSuccess: function(data) {
    this.loading = false;
    this.gallery = extractGallery(data);
    this.emit("change");
  },

  onLoadFail: function() {
    this.loading = false;
    this.error = true;
    this.emit("change");
  },
});

module.exports = StartPageStore;