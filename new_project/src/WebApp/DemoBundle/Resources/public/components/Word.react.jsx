/** @jsx React.DOM */

var React = require("react");

var Word = React.createClass({
  render: function() {
    var statusText, statusStyle = {};
    switch(this.props.word.status) {
    case "OK":
      statusText = "";
      break;
    case "ADDING":
      statusText = "adding...";
      statusStyle = { color: "#ccc" };
      break;
    case "ERROR":
      statusText = "error: " + this.props.word.error;
      statusStyle = { color: "red" };
      break;
    }

    return (
      <li key={this.props.word.word}>
        {this.props.word.word} <span style={statusStyle}>{statusText}</span>
      </li>
    );
  }
});

module.exports = Word;