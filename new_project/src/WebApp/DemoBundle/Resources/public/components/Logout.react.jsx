/** @jsx React.DOM */

var React = require("react");

var AuthClient = require('../utils/AuthClient');

var Logout = React.createClass({
  componentDidMount: function() {
    AuthClient.logout();
  },

  render: function() {
    return <p>You are now logged out</p>;
  }
});

module.exports = Logout;