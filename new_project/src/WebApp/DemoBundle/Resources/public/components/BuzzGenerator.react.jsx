/** @jsx React.DOM */

var React = require("react"),
    Fluxxor = require("fluxxor");

var Word = require("./Word.react.jsx");

var FluxMixin = Fluxxor.FluxMixin(React),
    StoreWatchMixin = Fluxxor.StoreWatchMixin;

var BuzzGenerator = React.createClass({
  mixins: [FluxMixin, StoreWatchMixin("BuzzwordStore")],

  getInitialState: function() {
    return {
      suggestBuzzword: ""
    };
  },

  getStateFromFlux: function() {
    var store = this.getFlux().store("BuzzwordStore");
    return {
      loading: store.loading,
      error: store.error,
      words: store.words
    };
  },

  render: function() {
    return (
      <div>
        <h1>All the Buzzwords</h1>
        {this.state.error ? "Error loading data" : null}
        <ul style={{lineHeight: "1.3em", minHeight: "13em"}}>
          {this.state.loading ? <li>Loading...</li> : null}
          {this.state.words.map(function(word) {
            return <Word word={word} />;
          })}
        </ul>
        <h2>Suggest a New Buzzword</h2>
        <form onSubmit={this.handleSubmitForm}>
          <input type="text" value={this.state.suggestBuzzword}
                 onChange={this.handleSuggestedWordChange} />
          <input type="submit" value="Add" />
        </form>
      </div>
    );
  },

  componentDidMount: function() {
    this.getFlux().actions.buzz.loadBuzz();
  },

  handleSuggestedWordChange: function(e) {
    this.setState({suggestBuzzword: e.target.value});
  },

  handleSubmitForm: function(e) {
    e.preventDefault();
    if (this.state.suggestBuzzword.trim()) {
      this.getFlux().actions.buzz.addBuzz(this.state.suggestBuzzword);
      this.setState({suggestBuzzword: ""});
    }
  }
});

module.exports = BuzzGenerator;