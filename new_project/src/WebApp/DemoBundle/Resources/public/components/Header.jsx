/** @jsx React.DOM */

var React = require("react");
var Router = require('react-router'),
    Navigation = Router.Navigation,
    Link = Router.Link;

var Menu = React.createClass({
      render: function(){

          return (
              <div className="menu">
                  <nav>
                      <ul className="nav nav-pills">
                          <li><Link to="start">Home</Link></li>
                          <li><Link to="hello">Hello</Link></li>
                          <li><Link to="world">World</Link></li>
                      </ul>
                  </nav>
              </div>
          );

      }

    });

module.exports = Menu; 

var Header = React.createClass({

    render: function () {
            return (
                <header className="header">
                    <nav className="topNavi">
                        <Menu />
                    </nav>
                </header>
            );
    }
});

module.exports = Header