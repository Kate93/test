var keyMirror = require('react/lib/keyMirror');

module.exports = keyMirror({
    LOAD_CONTENT_START: null,
    LOAD_CONTENT_START_SUCCESS: null,
    LOAD_CONTENT_START_FAIL: null,

    LOAD_CONTENT_FOOTER: null,
    LOAD_CONTENT_FOOTER_SUCCESS: null,
    LOAD_CONTENT_FOOTER_FAIL: null
});