<?php
/**
 * Created by PhpStorm.
 * User: Kate
 * Date: 12/1/2014
 * Time: 1:49 PM
 */

namespace WebApp\DemoBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class WebAppController extends Controller {

    public function indexAction() {

        return $this->render('WebAppDemoBundle:Index:index.html.twig');
    }

} 